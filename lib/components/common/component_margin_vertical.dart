import 'package:flutter/material.dart';
import 'package:see_you_app/enums/enum_size.dart';

class ComponentMarginVertical extends StatelessWidget {
  final EnumSize enumSize;

  const ComponentMarginVertical({Key? key, this.enumSize = EnumSize.small}) : super(key: key);

  double _getSize() {
    switch(enumSize) {
      case EnumSize.micro:
        return 5;
        break;
      case EnumSize.small:
        return 10;
        break;
      case EnumSize.mid:
        return 20;
        break;
      case EnumSize.big:
        return 30;
        break;
      case EnumSize.bigger:
        return 40;
        break;
    }

    return 5;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: _getSize(),
    );
  }
}
