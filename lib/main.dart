import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:see_you_app/login_check.dart';
import 'package:see_you_app/pages/page_attendance.dart';
import 'package:see_you_app/pages/page_login.dart';
import 'package:see_you_app/pages/page_stock.dart';

import 'config/config_color.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'seeyoucoffee demo',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],

      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: colorBackGround,
      ),
      home: LoginCheck(),         // 로그인 체크 - 완료
      //home: PageHome(),         //
      // home: PageKickBoard(pageTitleText: "킥보드 상세/변경", isUpdateMode: true),
      //home: PageKickBoard(pageTitleText: "킥보드 등록", isUpdateMode: false),

    );
  }
}