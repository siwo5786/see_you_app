import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';

class PageSearchTest extends StatefulWidget {
  const PageSearchTest({Key? key}) : super(key: key);

  @override
  State<PageSearchTest> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<PageSearchTest> {
  String searchValue = '';
  final List<String> _suggestions = [
  ];

  Future<List<String>> _fetchSuggestions(String searchValue) async {
    await Future.delayed(const Duration(milliseconds: 750));

    return _suggestions.where((element) {
      return element.toLowerCase().contains(searchValue.toLowerCase());
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Example',
        theme: ThemeData(primarySwatch: Colors.orange),
        home: Scaffold(
            appBar: EasySearchBar(
                title: const Text('Example'),
                onSearch: (value) => setState(() => searchValue = value),
                asyncSuggestions: (value) async =>
                await _fetchSuggestions(value)),

            body: Center(child: Text('Value: $searchValue'))));
  }
}