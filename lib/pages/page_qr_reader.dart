import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/components/common/component_text_btn.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/enums/enum_size.dart';

class PageQrReader extends StatefulWidget {
  const PageQrReader({super.key});

  @override
  State<PageQrReader> createState() => _PageQrReaderState();
}

class _PageQrReaderState extends State<PageQrReader> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: '근태'),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    // if (currentResult?.text != null) {
      return Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width, // 사진 꽉 차게
              height: 200,
              color: Colors.white,
              child: Image.asset('assets/login_bg.jpg', fit: BoxFit.cover), // 비율이 변경되지 않고 꽉 채움. 이미지 잘릴 수 있음.
            ),
            const ComponentMarginVertical(enumSize: EnumSize.big),
            const Center(
              child: Text(
                "킥보드를 사용하시려면 사용 시작 버튼을 눌러주세요.",
                style: TextStyle(
                  fontSize: fontSizeBig
                ),
              ),
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                text:'사용 시작',
                callback: () {},
              ),
            ),
          ],
        ),
      );
    }
    // else {
    //   return QRCodeDartScanView(
    //     scanInvertedQRCode: true,
    //     onCapture: (Result result) {
    //       setState(() {
    //         currentResult = result;
    //       });
    //     },
    //     child: Align(
    //       alignment: Alignment.bottomCenter,
    //       child: Container(
    //         margin: const EdgeInsets.all(20),
    //         padding: const EdgeInsets.all(20),
    //         decoration: BoxDecoration(
    //           color: Colors.white,
    //           borderRadius: BorderRadius.circular(20),
    //         ),
    //         child: Column(
    //           mainAxisSize: MainAxisSize.min,
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: [
    //             Text(currentResult?.text ?? '킥보드 상단 QR코드를 찍으세요'),
    //           ],
    //         ),
    //       ),
    //     ),
    //   );
    // }

}
