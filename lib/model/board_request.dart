class BoardRequest {
  String title;
  String content;

  BoardRequest(this.title, this.content);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['title'] = title;
    data['content'] = content;

    return data;
  }
}